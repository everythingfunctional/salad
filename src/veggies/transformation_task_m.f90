module veggies_transformation_task_m
    use payload_m, only: payload_t
    use task_m, only: task_t
    use veggies_input_m, only: input_t, deserialize_i
    use veggies_test_interfaces_m, only: transformer_i
    use veggies_transformed_m, only: transformed_t

    implicit none
    private
    public :: transformation_task_t

    type, extends(task_t) :: transformation_task_t
        private
        procedure(deserialize_i), nopass, pointer :: deserializer
        procedure(transformer_i), nopass, pointer :: transformer
    contains
        procedure :: execute
    end type

    interface transformation_task_t
        module procedure constructor
    end interface
contains
    function constructor(deserializer, transformer) result(transformation_task)
        procedure(deserialize_i), pointer, intent(in) :: deserializer
        procedure(transformer_i), pointer, intent(in) :: transformer
        type(transformation_task_t) :: transformation_task

        transformation_task%deserializer => deserializer
        transformation_task%transformer => transformer
    end function

    function execute(self, arguments) result(output)
        class(transformation_task_t), intent(in) :: self
        type(payload_t), intent(in) :: arguments(:)
        type(payload_t) :: output

        class(input_t), allocatable :: input
        type(transformed_t) :: transformed_
        class(input_t), allocatable :: transformed_input

        input = self%deserializer(arguments(1)%string_payload())
        transformed_ = self%transformer(input)
        transformed_input = transformed_%input()
        output = payload_t(transformed_input%serialize())
    end function
end module
