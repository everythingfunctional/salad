module veggies_double_precision_input_m
    use veggies_input_m, only: input_t, deserialize_i

    implicit none
    private
    public :: double_precision_input_t

    type, extends(input_t) :: double_precision_input_t
        private
        double precision :: input_
    contains
        private
        procedure, public :: input
        procedure, public :: serialize
        procedure, public :: get_deserializer
    end type

    interface double_precision_input_t
        module procedure constructor
    end interface
contains
    pure function constructor(input) result(double_precision_input)
        double precision, intent(in) :: input
        type(double_precision_input_t) :: double_precision_input

        double_precision_input%input_ = input
    end function

    pure function input(self)
        class(double_precision_input_t), intent(in) :: self
        double precision :: input

        input = self%input_
    end function

    pure function serialize(self) result(string)
        class(double_precision_input_t), intent(in) :: self
        character(len=:), allocatable :: string

        allocate(character(len=34) :: string)
        write(string, *) self%input_
    end function

    pure function deserialize(string) result(input)
        character(len=*), intent(in) :: string
        class(input_t), allocatable :: input

        type(double_precision_input_t) :: local_input

        read(string, *) local_input%input_
        input = local_input
    end function

    pure function get_deserializer(self) result(deserializer)
        class(double_precision_input_t), intent(in) :: self
        procedure(deserialize_i), pointer :: deserializer

        associate(unused => self); end associate
        deserializer => deserialize
    end function
end module
