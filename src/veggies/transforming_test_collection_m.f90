module veggies_transforming_test_collection_m
    use iso_varying_string, only: varying_string, operator(//), char, put_line
    use strff, only: operator(.includes.), add_hanging_indentation, join, NEWLINE
    use task_item_m, only: task_item_t
    use veggies_bracket_task_m, only: bracket_task_t
    use veggies_command_line_m, only: DEBUG
    use veggies_constants_m, only: INDENTATION
    use veggies_input_m, only: input_t, deserialize_i
    use veggies_result_m, only: fail
    use veggies_result_collection_task_m, only: result_collection_task_t
    use veggies_test_m, only: &
            filter_result_t, test_t, filter_failed, filter_matched
    use veggies_test_case_result_m, only: test_case_result_t
    use veggies_test_collection_result_m, only: test_collection_result_t
    use veggies_test_interfaces_m, only: computation_i, transformer_i
    use veggies_test_item_m, only: filter_item_result_t, test_item_t
    use veggies_test_result_item_m, only: test_result_item_t
    use veggies_transformation_failure_m, only: transformation_failure_t
    use veggies_transformation_task_m, only: transformation_task_t
    use veggies_transformed_m, only: transformed_t
    use vertex_m, only: vertex_t

    implicit none
    private
    public :: transforming_test_collection_t

    type, extends(test_t) :: transforming_test_collection_t
        private
        type(varying_string) :: description_
        type(test_item_t), allocatable :: tests(:)
        procedure(transformer_i), nopass, pointer :: transformer
        procedure(deserialize_i), nopass, pointer :: transformed_deserializer
        logical :: has_setup_and_teardown
        procedure(computation_i), nopass, pointer :: setup
        procedure(computation_i), nopass, pointer :: teardown
    contains
        private
        procedure, public :: description
        procedure, public :: filter
        procedure, public :: num_cases
        procedure, public :: run_with_input
        procedure, public :: run_without_input
        procedure, public :: add_tasks
    end type

    interface transforming_test_collection_t
        module procedure constructor_basic
        module procedure constructor_bracketed
    end interface
contains
    function constructor_basic( &
            description, transformer, transformed_deserializer, tests) result(transforming_test_collection)
        type(varying_string), intent(in) :: description
        procedure(transformer_i), pointer, intent(in) :: transformer
        procedure(deserialize_i), pointer, intent(in) :: transformed_deserializer
        type(test_item_t), intent(in) :: tests(:)
        type(transforming_test_collection_t) :: transforming_test_collection

        transforming_test_collection%description_ = description
        transforming_test_collection%transformer => transformer
        transforming_test_collection%transformed_deserializer => transformed_deserializer
        allocate(transforming_test_collection%tests, source = tests)
        transforming_test_collection%has_setup_and_teardown = .false.
    end function

    function constructor_bracketed( &
            description, &
            transformer, &
            transformed_deserializer, &
            tests, &
            setup, &
            teardown) &
            result(transforming_test_collection)
        type(varying_string), intent(in) :: description
        procedure(transformer_i), pointer, intent(in) :: transformer
        procedure(deserialize_i), pointer, intent(in) :: transformed_deserializer
        type(test_item_t), intent(in) :: tests(:)
        procedure(computation_i), pointer, intent(in) :: setup
        procedure(computation_i), pointer, intent(in) :: teardown
        type(transforming_test_collection_t) :: transforming_test_collection

        transforming_test_collection%description_ = description
        transforming_test_collection%transformer => transformer
        transforming_test_collection%transformed_deserializer => transformed_deserializer
        allocate(transforming_test_collection%tests, source = tests)
        transforming_test_collection%has_setup_and_teardown = .true.
        transforming_test_collection%setup => setup
        transforming_test_collection%teardown => teardown
    end function

    pure recursive function description(self)
        class(transforming_test_collection_t), intent(in) :: self
        type(varying_string) :: description

        integer :: i

        description = add_hanging_indentation( &
                self%description_ // NEWLINE // join( &
                        [(self%tests(i)%description(), i = 1, size(self%tests))], &
                        NEWLINE), &
                INDENTATION)
    end function

    recursive function filter(self, filter_string) result(filter_result)
        class(transforming_test_collection_t), intent(in) :: self
        type(varying_string), intent(in) :: filter_string
        type(filter_result_t) :: filter_result

        type(transforming_test_collection_t) :: new_collection
        type(filter_item_result_t) :: filter_results(size(self%tests))
        integer :: i
        logical :: matches(size(self%tests))
        type(test_item_t) :: maybe_tests(size(self%tests))

        if (self%description_.includes.filter_string) then
            filter_result = filter_matched(self)
        else
            filter_results = [(self%tests(i)%filter(filter_string), i = 1, size(self%tests))]
            if (any(filter_results%matched())) then
                matches = filter_results%matched()
                maybe_tests = filter_results%test()
                new_collection = self
                deallocate(new_collection%tests)
                allocate(new_collection%tests, source = &
                        pack(maybe_tests, mask=matches))
                filter_result = filter_matched(new_collection)
            else
                filter_result = filter_failed()
            end if
        end if
    end function

    pure recursive function num_cases(self)
        class(transforming_test_collection_t), intent(in) :: self
        integer :: num_cases

        integer :: i

        num_cases = sum([(self%tests(i)%num_cases(), i = 1, size(self%tests))])
    end function

    recursive function run_with_input(self, input) result(result_)
        class(transforming_test_collection_t), intent(in) :: self
        class(input_t), intent(in) :: input
        type(test_result_item_t) :: result_

        integer :: i
        type(test_result_item_t) :: results(size(self%tests))
        type(transformed_t) :: transformed_

        if (DEBUG) call put_line("Beginning execution of: " // self%description_)
        if (self%has_setup_and_teardown) call self%setup
        transformed_ = self%transformer(input)
        select type (transformed_input => transformed_%input())
        type is (transformation_failure_t)
            result_ = test_result_item_t(test_case_result_t( &
                    self%description_, transformed_input%result_()))
        class default
            do i = 1, size(self%tests)
                results(i) = self%tests(i)%run(transformed_input)
            end do
            result_ = test_result_item_t(test_collection_result_t( &
                    self%description_, results))
        end select
        if (self%has_setup_and_teardown) call self%teardown
        if (DEBUG) call put_line("Completed execution of: " // self%description_)
    end function

    function run_without_input(self) result(result_)
        class(transforming_test_collection_t), intent(in) :: self
        type(test_result_item_t) :: result_

        if (DEBUG) call put_line("Improper use of: " // self%description_)
        result_ = test_result_item_t(test_case_result_t( &
                self%description_, fail("No input provided")))
    end function

    recursive subroutine add_tasks( &
            self, parent_task_nums, input_deserializer, tasks, vertices, child_task_nums)
        class(transforming_test_collection_t), intent(in) :: self
        integer, intent(in) :: parent_task_nums(:)
        procedure(deserialize_i), pointer, intent(in) :: input_deserializer
        type(task_item_t), allocatable, intent(inout) :: tasks(:)
        type(vertex_t), allocatable, intent(inout) :: vertices(:)
        integer, allocatable, intent(out) :: child_task_nums(:)

        integer :: i
        integer, allocatable :: my_child_task_nums(:)
        integer :: num_children
        integer :: setup_task_num
        integer, allocatable :: teardown_task_nums(:)
        integer :: transformation_task
        integer :: this_task

        if (associated(input_deserializer)) then
            num_children = size(self%tests)
            allocate(my_child_task_nums(num_children))
            allocate(teardown_task_nums(0))
            if (self%has_setup_and_teardown) then
                tasks = [tasks, task_item_t(bracket_task_t(self%setup))]
                if (size(parent_task_nums) == 1) then
                    vertices = [vertices, vertex_t([integer :: ], self%description_ // ": setup")]
                else
                    vertices = [vertices, vertex_t(parent_task_nums(2:), self%description_ // ": setup")]
                end if
                setup_task_num = size(vertices)
                tasks = [tasks, task_item_t(transformation_task_t(input_deserializer, self%transformer))]
                vertices = [vertices, vertex_t([parent_task_nums, setup_task_num], self%description_ // ": transformer")]
                transformation_task = setup_task_num + 1
                do i = 1, num_children
                    block
                        integer, allocatable :: local_task_nums(:)
                        if (size(parent_task_nums) == 1) then
                            call self%tests(i)%add_tasks( &
                                    [transformation_task, setup_task_num], &
                                    self%transformed_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                        else
                            call self%tests(i)%add_tasks( &
                                    [transformation_task, setup_task_num, parent_task_nums(2:)], &
                                    self%transformed_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                        end if
                        if (size(local_task_nums) > 1) then
                            teardown_task_nums = [teardown_task_nums, local_task_nums(2:)]
                        end if
                        my_child_task_nums(i) = local_task_nums(1)
                    end block
                end do
                tasks = [tasks, task_item_t(result_collection_task_t(self%description_))]
                vertices = [vertices, vertex_t(my_child_task_nums, self%description_)]
                this_task = size(vertices)
                tasks = [tasks, task_item_t(bracket_task_t(self%teardown))]
                vertices = [vertices, vertex_t(teardown_task_nums, self%description_ // ": teardown")]
                child_task_nums = [this_task, teardown_task_nums, size(vertices)]
            else
                tasks = [tasks, task_item_t(transformation_task_t(input_deserializer, self%transformer))]
                vertices = [vertices, vertex_t(parent_task_nums, self%description_ // ": transformer")]
                transformation_task = size(vertices)
                do i = 1, num_children
                    block
                        integer, allocatable :: local_task_nums(:)
                        if (size(parent_task_nums) == 1) then
                            call self%tests(i)%add_tasks( &
                                    [transformation_task], &
                                    self%transformed_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                        else
                            call self%tests(i)%add_tasks( &
                                    [transformation_task, parent_task_nums(2:)], &
                                    self%transformed_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                        end if
                        if (size(local_task_nums) > 1) then
                            teardown_task_nums = [teardown_task_nums, local_task_nums(2:)]
                        end if
                        my_child_task_nums(i) = local_task_nums(1)
                    end block
                end do
                tasks = [tasks, task_item_t(result_collection_task_t(self%description_))]
                vertices = [vertices, vertex_t(my_child_task_nums, self%description_)]
                this_task = size(vertices)
                child_task_nums = [this_task, teardown_task_nums]
            end if
        else
            error stop "No input provided to " // char(self%description_)
        end if
    end subroutine
end module
