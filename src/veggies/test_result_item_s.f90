submodule (veggies_test_result_item_m) veggies_test_result_item_s
    use veggies_test_case_result_m, only: test_case_result_t
    use veggies_test_collection_result_m, only: test_collection_result_t

    implicit none
contains
    module procedure deserialize
        select case (string(12:12))
        case ("S")
            test_result_item%result_ = test_case_result_t(string)
        case ("M")
            test_result_item%result_ = test_collection_result_t(string)
        end select
    end procedure
end submodule
