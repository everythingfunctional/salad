module veggies_generator_case_task_m
    use iso_varying_string, only: varying_string, operator(//), put_line
    use payload_m, only: payload_t
    use strff, only: to_string
    use task_m, only: task_t
    use veggies_command_line_m, only: &
            DEBUG, MAX_SHRINK_ATTEMPTS, NUM_GENERATOR_TESTS
    use veggies_generated_m, only: generated_t
    use veggies_generator_m, only: generator_t
    use veggies_result_m, only: result_t, fail, succeed
    use veggies_shrink_result_m, only: shrink_result_t
    use veggies_test_case_result_m, only: test_case_result_t
    use veggies_test_interfaces_m, only: computation_i, input_test_i
    use veggies_test_result_item_m, only: test_result_item_t

    implicit none
    private
    public :: generator_case_task_t

    type, extends(task_t) :: generator_case_task_t
        private
        type(varying_string) :: description_
        class(generator_t), allocatable :: generator
        procedure(input_test_i), nopass, pointer :: test
        logical :: has_setup_and_teardown
        procedure(computation_i), nopass, pointer :: setup
        procedure(computation_i), nopass, pointer :: teardown
    contains
        procedure :: execute
    end type

    interface generator_case_task_t
        module procedure constructor_basic
        module procedure constructor_bracketed
    end interface
contains
    function constructor_basic( &
            description, generator, test) result(generator_case_task)
        type(varying_string), intent(in) :: description
        class(generator_t), intent(in) :: generator
        procedure(input_test_i) :: test
        type(generator_case_task_t) :: generator_case_task

        generator_case_task%description_ = description
        allocate(generator_case_task%generator, source = generator)
        generator_case_task%test => test
        generator_case_task%has_setup_and_teardown = .false.
    end function

    function constructor_bracketed( &
            description, generator, test, setup, teardown) result(generator_case_task)
        type(varying_string), intent(in) :: description
        class(generator_t), intent(in) :: generator
        procedure(input_test_i) :: test
        procedure(computation_i) :: setup
        procedure(computation_i) :: teardown
        type(generator_case_task_t) :: generator_case_task

        generator_case_task%description_ = description
        allocate(generator_case_task%generator, source = generator)
        generator_case_task%test => test
        generator_case_task%has_setup_and_teardown = .true.
        generator_case_task%setup => setup
        generator_case_task%teardown => teardown
    end function

    function execute(self, arguments) result(output)
        class(generator_case_task_t), intent(in) :: self
        type(payload_t), intent(in) :: arguments(:)
        type(payload_t) :: output

        type(generated_t) :: generated_value
        integer :: i
        type(result_t) :: new_result
        type(result_t) :: previous_result
        type(test_result_item_t) :: result_
        type(shrink_result_t) :: simpler_value

        associate(unused => arguments)
        end associate

        if (DEBUG) call put_line("Beginning execution of: " // self%description_)
        if (self%has_setup_and_teardown) call self%setup
        do i = 1, NUM_GENERATOR_TESTS
            generated_value = self%generator%generate()
            previous_result = self%test(generated_value%input())
            if (.not.previous_result%passed()) exit
        end do
        search: if (i > NUM_GENERATOR_TESTS) then
            result_ = test_result_item_t(test_case_result_t( &
                    self%description_, &
                    succeed("Passed after " // to_string(NUM_GENERATOR_TESTS) // " examples")))
        else search
            do i = 1, MAX_SHRINK_ATTEMPTS
                simpler_value = self%generator%shrink(generated_value%input())
                new_result = self%test(simpler_value%input())
                if (simpler_value%simplest()) then
                    if (new_result%passed()) then
                        result_ = test_result_item_t(test_case_result_t( &
                                self%description_, &
                                fail('Found simplest example causing failure').and.previous_result))
                        exit search
                    else
                        result_ = test_result_item_t(test_case_result_t( &
                                self%description_, &
                                fail('Fails with the simplest possible example').and.new_result))
                        exit search
                    end if
                else
                    if (new_result%passed()) then
                        result_ = test_result_item_t(test_case_result_t( &
                                self%description_, &
                                fail('Found simplest example causing failure').and.previous_result))
                        exit search
                    else
                        previous_result = new_result
                        generated_value = generated_t(simpler_value%input())
                    end if
                end if
            end do
            result_ = test_result_item_t(test_case_result_t( &
                    self%description_, &
                    fail("Exhausted shrink attempts looking for simplest value causing failure").and.previous_result))
        end if search
        output = payload_t(result_%serialize())
        if (self%has_setup_and_teardown) call self%teardown
        if (DEBUG) call put_line("Completed execution of: " // self%description_)
    end function
end module
