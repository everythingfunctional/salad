module veggies_input_case_task_m
    use iso_varying_string, only: varying_string, operator(//), put_line
    use payload_m, only: payload_t
    use strff, only: to_string
    use task_m, only: task_t
    use veggies_command_line_m, only: DEBUG
    use veggies_input_m, only: input_t, deserialize_i
    use veggies_test_case_result_m, only: test_case_result_t
    use veggies_test_interfaces_m, only: computation_i, input_test_i
    use veggies_test_result_item_m, only: test_result_item_t

    implicit none
    private
    public :: input_case_task_t

    type, extends(task_t) :: input_case_task_t
        private
        type(varying_string) :: description_
        procedure(deserialize_i), nopass, pointer :: deserializer
        procedure(input_test_i), nopass, pointer :: test
        logical :: has_setup_and_teardown
        procedure(computation_i), nopass, pointer :: setup
        procedure(computation_i), nopass, pointer :: teardown
    contains
        procedure :: execute
    end type

    interface input_case_task_t
        module procedure constructor_basic
        module procedure constructor_bracketed
    end interface
contains
    function constructor_basic(description, deserializer, test) result(input_case_task)
        type(varying_string), intent(in) :: description
        procedure(deserialize_i), pointer, intent(in) :: deserializer
        procedure(input_test_i), pointer, intent(in) :: test
        type(input_case_task_t) :: input_case_task

        input_case_task%description_ = description
        input_case_task%deserializer => deserializer
        input_case_task%test => test
        input_case_task%has_setup_and_teardown = .false.
    end function

    function constructor_bracketed( &
            description, deserializer, test, setup, teardown) result(input_case_task)
        type(varying_string), intent(in) :: description
        procedure(deserialize_i), pointer, intent(in) :: deserializer
        procedure(input_test_i), pointer, intent(in) :: test
        procedure(computation_i), pointer, intent(in) :: setup
        procedure(computation_i), pointer, intent(in) :: teardown
        type(input_case_task_t) :: input_case_task

        input_case_task%description_ = description
        input_case_task%deserializer => deserializer
        input_case_task%test => test
        input_case_task%has_setup_and_teardown = .true.
        input_case_task%setup => setup
        input_case_task%teardown => teardown
    end function

    function execute(self, arguments) result(output)
        class(input_case_task_t), intent(in) :: self
        type(payload_t), intent(in) :: arguments(:)
        type(payload_t) :: output

        type(test_result_item_t) :: result_
        class(input_t), allocatable :: input

        input = self%deserializer(arguments(1)%string_payload())
        if (DEBUG) call put_line( &
                "Beginning execution of: " // self%description_ &
                // " on image " // to_string(this_image()))
        if (self%has_setup_and_teardown) call self%setup
        result_ = test_result_item_t(test_case_result_t( &
                self%description_, self%test(input)))
        output = payload_t(result_%serialize())
        if (self%has_setup_and_teardown) call self%teardown
        if (DEBUG) call put_line( &
                "Completed execution of: " // self%description_ &
                // " on image " // to_string(this_image()))
    end function
end module
