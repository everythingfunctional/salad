module veggies_result_collection_task_m
    use iso_varying_string, only: varying_string
    use payload_m, only: payload_t
    use task_m, only: task_t
    use veggies_test_collection_result_m, only: test_collection_result_t
    use veggies_test_result_item_m, only: test_result_item_t

    implicit none
    private
    public :: result_collection_task_t

    type, extends(task_t) :: result_collection_task_t
        type(varying_string) :: description
    contains
        procedure :: execute
    end type

    interface result_collection_task_t
        module procedure constructor
    end interface
contains
    function constructor(description) result(result_collection_task)
        type(varying_string), intent(in) :: description
        type(result_collection_task_t) :: result_collection_task

        result_collection_task%description = description
    end function

    function execute(self, arguments) result(output)
        class(result_collection_task_t), intent(in) :: self
        type(payload_t), intent(in) :: arguments(:)
        type(payload_t) :: output

        type(test_result_item_t) :: combined
        integer :: i

        combined = test_result_item_t(test_collection_result_t( &
                self%description, &
                [(test_result_item_t(arguments(i)%string_payload()), i = 1, size(arguments))]))
        output = payload_t(combined%serialize())
    end function
end module
