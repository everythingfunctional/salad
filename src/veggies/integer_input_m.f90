module veggies_integer_input_m
    use veggies_input_m, only: input_t, deserialize_i

    implicit none
    private
    public :: integer_input_t

    type, extends(input_t) :: integer_input_t
        private
        integer :: input_
    contains
        private
        procedure, public :: input
        procedure, public :: serialize
        procedure, public :: get_deserializer
    end type

    interface integer_input_t
        module procedure constructor
    end interface
contains
    pure function constructor(input) result(integer_input)
        integer, intent(in) :: input
        type(integer_input_t) :: integer_input

        integer_input%input_ = input
    end function

    pure function input(self)
        class(integer_input_t), intent(in) :: self
        integer :: input

        input = self%input_
    end function

    pure function serialize(self) result(string)
        class(integer_input_t), intent(in) :: self
        character(len=:), allocatable :: string

        allocate(character(len=11) :: string)
        write(string, '(I11)') self%input_
    end function

    pure function deserialize(string) result(input)
        character(len=*), intent(in) :: string
        class(input_t), allocatable :: input

        type(integer_input_t) :: local_input

        read(string, '(I11)') local_input%input_
        input = local_input
    end function

    pure function get_deserializer(self) result(deserializer)
        class(integer_input_t), intent(in) :: self
        procedure(deserialize_i), pointer :: deserializer

        associate(unused => self); end associate
        deserializer => deserialize
    end function
end module
