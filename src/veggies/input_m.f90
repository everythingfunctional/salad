module veggies_input_m
    implicit none
    private
    public :: input_t, deserialize_i

    type, abstract :: input_t
    contains
        procedure(serialize_i), deferred :: serialize
        procedure(get_deserializer_i), deferred :: get_deserializer
    end type

    abstract interface
        function serialize_i(self) result(string)
            import :: input_t

            implicit none

            class(input_t), intent(in) :: self
            character(len=:), allocatable :: string
        end function

        function deserialize_i(string) result(input)
            import :: input_t

            implicit none

            character(len=*), intent(in) :: string
            class(input_t), allocatable :: input
        end function

        function get_deserializer_i(self) result(deserializer)
            import :: deserialize_i, input_t

            implicit none

            class(input_t), intent(in) :: self
            procedure(deserialize_i), pointer :: deserializer
        end function
    end interface
end module
