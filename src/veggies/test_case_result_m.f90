module veggies_test_case_result_m
    use iso_varying_string, only: &
            varying_string, assignment(=), operator(//), char, len
    use strff, only: add_hanging_indentation, NEWLINE
    use veggies_constants_m, only: INDENTATION
    use veggies_result_m, only: result_t
    use veggies_test_result_m, only: test_result_t

    implicit none
    private
    public :: test_case_result_t

    type, extends(test_result_t) :: test_case_result_t
        private
        type(varying_string) :: description
        type(result_t) :: result_
    contains
        private
        procedure, public :: num_asserts
        procedure, public :: num_cases
        procedure, public :: num_failing_asserts
        procedure, public :: num_failing_cases
        procedure, public :: passed
        procedure, public :: failure_description
        procedure, public :: verbose_description
        procedure, public :: serialize
    end type

    interface test_case_result_t
        module procedure constructor
        module procedure deserialize
    end interface
contains
    pure function constructor(description, result_) result(test_case_result)
        type(varying_string), intent(in) :: description
        type(result_t), intent(in) :: result_
        type(test_case_result_t) :: test_case_result

        test_case_result%description = description
        test_case_result%result_ = result_
    end function

    pure function deserialize(string) result(test_case_result)
        ! Format
        ! total_length, type_tag, description_length, description, result
        character(len=*), intent(in) :: string
        type(test_case_result_t) :: test_case_result

        integer :: description_length

        read(string(13:23), '(I11)') description_length
        test_case_result%description = string(24:description_length+23)
        test_case_result%result_ = result_t(string(description_length+24:))
    end function

    pure function failure_description( &
            self, colorize) result(description)
        class(test_case_result_t), intent(in) :: self
        logical, intent(in) :: colorize
        type(varying_string) :: description

        if (self%passed()) then
            description = ""
        else
            description = add_hanging_indentation( &
                    self%description // NEWLINE &
                        // self%result_%failure_description(colorize), &
                    INDENTATION)
        end if
    end function

    pure function num_asserts(self)
        class(test_case_result_t), intent(in) :: self
        integer :: num_asserts

        num_asserts = self%result_%num_asserts()
    end function

    pure function num_cases(self)
        class(test_case_result_t), intent(in) :: self
        integer :: num_cases

        associate(unused => self)
        end associate

        num_cases = 1
    end function

    pure function num_failing_asserts(self) result(num_asserts)
        class(test_case_result_t), intent(in) :: self
        integer :: num_asserts

        num_asserts = self%result_%num_failing_asserts()
    end function

    pure function num_failing_cases(self) result(num_cases)
        class(test_case_result_t), intent(in) :: self
        integer :: num_cases

        if (self%passed()) then
            num_cases = 0
        else
            num_cases = 1
        end if
    end function

    pure function passed(self)
        class(test_case_result_t), intent(in) :: self
        logical :: passed

        passed = self%result_%passed()
    end function

    pure function verbose_description( &
            self, colorize) result(description)
        class(test_case_result_t), intent(in) :: self
        logical, intent(in) :: colorize
        type(varying_string) :: description

        description = add_hanging_indentation( &
                self%description // NEWLINE &
                    // self%result_%verbose_description(colorize), &
                INDENTATION)
    end function

    pure function serialize(self) result(string)
        ! Format
        ! total_length, type_tag, description_length, description, result
        class(test_case_result_t), intent(in) :: self
        character(len=:), allocatable :: string

        character(len=*), parameter :: type_tag = "S"
        integer :: description_length
        character(len=11) :: description_length_part
        character(len=:), allocatable :: serialized_result
        integer :: total_length
        character(len=11) :: total_length_part

        serialized_result = self%result_%serialize()
        description_length = len(self%description)
        total_length = 11*2 + 1 + description_length + len(serialized_result)
        write(description_length_part, '(I11)') description_length
        write(total_length_part, '(I11)') total_length

        string = &
                total_length_part &
                // type_tag &
                // description_length_part &
                // char(self%description) &
                // serialized_result
    end function
end module
