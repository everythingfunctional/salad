module veggies_bracket_task_m
    use payload_m, only: payload_t
    use task_m, only: task_t
    use veggies_test_interfaces_m, only: computation_i

    implicit none
    private
    public :: bracket_task_t

    type, extends(task_t) :: bracket_task_t
        private
        procedure(computation_i), nopass, pointer :: action
    contains
        procedure :: execute
    end type

    interface bracket_task_t
        module procedure constructor
    end interface
contains
    function constructor(action) result(bracket_task)
        procedure(computation_i), pointer, intent(in) :: action
        type(bracket_task_t) :: bracket_task

        bracket_task%action => action
    end function

    function execute(self, arguments) result(output)
        class(bracket_task_t), intent(in) :: self
        type(payload_t), intent(in) :: arguments(:)
        type(payload_t) :: output

        associate(unused => arguments)
        end associate
        call self%action()
        output = payload_t()
    end function
end module
