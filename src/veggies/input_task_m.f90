module veggies_input_task_m
    use payload_m, only: payload_t
    use task_m, only: task_t
    use veggies_input_m, only: input_t

    implicit none
    private
    public :: input_task_t

    type, extends(task_t) :: input_task_t
        private
        class(input_t), allocatable :: input
    contains
        procedure :: execute
    end type

    interface input_task_t
        module procedure constructor
    end interface
contains
    function constructor(input) result(input_task)
        class(input_t), intent(in) :: input
        type(input_task_t) :: input_task

        input_task%input = input
    end function

    function execute(self, arguments) result(output)
        class(input_task_t), intent(in) :: self
        type(payload_t), intent(in) :: arguments(:)
        type(payload_t) :: output

        associate(unused => arguments)
        end associate

        output = payload_t(self%input%serialize())
    end function
end module
