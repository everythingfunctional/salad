module veggies_test_collection_with_input_m
    use iso_varying_string, only: varying_string, operator(//), put_line
    use strff, only: operator(.includes.), add_hanging_indentation, join, NEWLINE
    use task_item_m, only: task_item_t
    use veggies_bracket_task_m, only: bracket_task_t
    use veggies_command_line_m, only: DEBUG
    use veggies_constants_m, only: INDENTATION
    use veggies_input_m, only: input_t, deserialize_i
    use veggies_input_task_m, only: input_task_t
    use veggies_result_collection_task_m, only: result_collection_task_t
    use veggies_test_m, only: &
            filter_result_t, test_t, filter_failed, filter_matched
    use veggies_test_collection_result_m, only: test_collection_result_t
    use veggies_test_interfaces_m, only: computation_i
    use veggies_test_item_m, only: filter_item_result_t, test_item_t
    use veggies_test_result_item_m, only: test_result_item_t
    use vertex_m, only: vertex_t

    implicit none
    private
    public :: test_collection_with_input_t

    type, extends(test_t) :: test_collection_with_input_t
        private
        type(varying_string) :: description_
        type(test_item_t), allocatable :: tests(:)
        class(input_t), allocatable :: input
        logical :: has_setup_and_teardown
        procedure(computation_i), nopass, pointer :: setup
        procedure(computation_i), nopass, pointer :: teardown
    contains
        private
        procedure, public :: description
        procedure, public :: filter
        procedure, public :: num_cases
        procedure, public :: run_with_input
        procedure, public :: run_without_input
        procedure, public :: add_tasks
    end type

    interface test_collection_with_input_t
        module procedure constructor_basic
        module procedure constructor_bracketed
    end interface
contains
    function constructor_basic( &
            description, input, tests) result(test_collection_with_input)
        type(varying_string), intent(in) :: description
        class(input_t), intent(in) :: input
        type(test_item_t), intent(in) :: tests(:)
        type(test_collection_with_input_t) :: test_collection_with_input

        test_collection_with_input%description_ = description
        allocate(test_collection_with_input%input, source = input)
        allocate(test_collection_with_input%tests, source = tests)
        test_collection_with_input%has_setup_and_teardown = .false.
    end function

    function constructor_bracketed( &
            description, input, tests, setup, teardown) result(test_collection_with_input)
        type(varying_string), intent(in) :: description
        class(input_t), intent(in) :: input
        type(test_item_t), intent(in) :: tests(:)
        procedure(computation_i) :: setup
        procedure(computation_i) :: teardown
        type(test_collection_with_input_t) :: test_collection_with_input

        test_collection_with_input%description_ = description
        allocate(test_collection_with_input%input, source = input)
        allocate(test_collection_with_input%tests, source = tests)
        test_collection_with_input%has_setup_and_teardown = .true.
        test_collection_with_input%setup => setup
        test_collection_with_input%teardown => teardown
    end function

    pure recursive function description(self)
        class(test_collection_with_input_t), intent(in) :: self
        type(varying_string) :: description

        integer :: i

        description = add_hanging_indentation( &
                self%description_ // NEWLINE // join( &
                        [(self%tests(i)%description(), i = 1, size(self%tests))], &
                        NEWLINE), &
                INDENTATION)
    end function

    recursive function filter(self, filter_string) result(filter_result)
        class(test_collection_with_input_t), intent(in) :: self
        type(varying_string), intent(in) :: filter_string
        type(filter_result_t) :: filter_result

        type(filter_item_result_t) :: filter_results(size(self%tests))
        integer :: i

        if (self%description_.includes.filter_string) then
            filter_result = filter_matched(self)
        else
            filter_results = [(self%tests(i)%filter(filter_string), i = 1, size(self%tests))]
            if (any(filter_results%matched())) then
                filter_result = filter_matched(test_collection_with_input_t(&
                        self%description_, &
                        self%input, &
                        pack(filter_results%test(), mask=filter_results%matched())))
            else
                filter_result = filter_failed()
            end if
        end if
    end function

    pure recursive function num_cases(self)
        class(test_collection_with_input_t), intent(in) :: self
        integer :: num_cases

        integer :: i

        num_cases = sum([(self%tests(i)%num_cases(), i = 1, size(self%tests))])
    end function

    recursive function run_with_input(self, input) result(result_)
        class(test_collection_with_input_t), intent(in) :: self
        class(input_t), intent(in) :: input
        type(test_result_item_t) :: result_

        associate(unused => input)
        end associate

        result_ = self%run()
    end function

    recursive function run_without_input(self) result(result_)
        class(test_collection_with_input_t), intent(in) :: self
        type(test_result_item_t) :: result_

        integer :: i
        type(test_result_item_t) :: results(size(self%tests))

        if (DEBUG) call put_line("Beginning execution of: " // self%description_)
        if (self%has_setup_and_teardown) call self%setup
        do i = 1, size(self%tests)
            results(i) = self%tests(i)%run(self%input)
        end do
        result_ = test_result_item_t(test_collection_result_t( &
                self%description_, results))
        if (self%has_setup_and_teardown) call self%teardown
        if (DEBUG) call put_line("Completed execution of: " // self%description_)
    end function

    recursive subroutine add_tasks( &
            self, parent_task_nums, input_deserializer, tasks, vertices, child_task_nums)
        class(test_collection_with_input_t), intent(in) :: self
        integer, intent(in) :: parent_task_nums(:)
        procedure(deserialize_i), pointer, intent(in) :: input_deserializer
        type(task_item_t), allocatable, intent(inout) :: tasks(:)
        type(vertex_t), allocatable, intent(inout) :: vertices(:)
        integer, allocatable, intent(out) :: child_task_nums(:)

        procedure(deserialize_i), pointer :: children_deserializer
        integer :: i
        integer :: input_task_num
        integer, allocatable :: my_child_task_nums(:)
        integer :: num_children
        integer :: setup_task_num
        integer, allocatable :: teardown_task_nums(:)
        integer :: this_task

        num_children = size(self%tests)
        allocate(my_child_task_nums(num_children))
        allocate(teardown_task_nums(0))
        tasks = [tasks, task_item_t(input_task_t(self%input))]
        vertices = [vertices, vertex_t([integer ::], self%description_ // ": input")]
        input_task_num = size(vertices)
        children_deserializer => self%input%get_deserializer()
        if (self%has_setup_and_teardown) then
            tasks = [tasks, task_item_t(bracket_task_t(self%setup))]
            setup_task_num = input_task_num + 1
            if (associated(input_deserializer)) then
                if (size(parent_task_nums) == 1) then
                    vertices = [vertices, vertex_t([integer ::], self%description_ // ": setup")]
                    do i = 1, num_children
                        block
                            integer, allocatable :: local_task_nums(:)
                            call self%tests(i)%add_tasks( &
                                    [input_task_num, setup_task_num], &
                                    children_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                            if (size(local_task_nums) > 1) then
                                teardown_task_nums = [teardown_task_nums, local_task_nums(2:)]
                            end if
                            my_child_task_nums(i) = local_task_nums(1)
                        end block
                    end do
                else
                    vertices = [vertices, vertex_t(parent_task_nums(2:), self%description_ // ": setup")]
                    do i = 1, num_children
                        block
                            integer, allocatable :: local_task_nums(:)
                            call self%tests(i)%add_tasks( &
                                    [input_task_num, setup_task_num, parent_task_nums(2:)], &
                                    children_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                            if (size(local_task_nums) > 1) then
                                teardown_task_nums = [teardown_task_nums, local_task_nums(2:)]
                            end if
                            my_child_task_nums(i) = local_task_nums(1)
                        end block
                    end do
                end if
            else
                vertices = [vertices, vertex_t(parent_task_nums, self%description_ // ": setup")]
                do i = 1, num_children
                    block
                        integer, allocatable :: local_task_nums(:)
                        call self%tests(i)%add_tasks( &
                        [input_task_num, setup_task_num, parent_task_nums], &
                        children_deserializer, &
                        tasks, &
                        vertices, &
                        local_task_nums)
                        if (size(local_task_nums) > 1) then
                            teardown_task_nums = [teardown_task_nums, local_task_nums(2:)]
                        end if
                        my_child_task_nums(i) = local_task_nums(1)
                    end block
                end do
            end if
        else
            do i = 1, num_children
                block
                    integer, allocatable :: local_task_nums(:)
                    if (associated(input_deserializer)) then
                        if (size(parent_task_nums) == 1) then
                            call self%tests(i)%add_tasks( &
                                    [input_task_num], &
                                    children_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                        else
                            call self%tests(i)%add_tasks( &
                                    [input_task_num, parent_task_nums(2:)], &
                                    children_deserializer, &
                                    tasks, &
                                    vertices, &
                                    local_task_nums)
                        end if
                    else
                        call self%tests(i)%add_tasks( &
                                [input_task_num, parent_task_nums], &
                                children_deserializer, &
                                tasks, &
                                vertices, &
                                local_task_nums)
                    end if
                    if (size(local_task_nums) > 1) then
                        teardown_task_nums = [teardown_task_nums, local_task_nums(2:)]
                    end if
                    my_child_task_nums(i) = local_task_nums(1)
                end block
            end do
        end if
        tasks = [tasks, task_item_t(result_collection_task_t(self%description_))]
        vertices = [vertices, vertex_t(my_child_task_nums, self%description_)]
        this_task = size(vertices)
        if (self%has_setup_and_teardown) then
            tasks = [tasks, task_item_t(bracket_task_t(self%teardown))]
            vertices = [vertices, vertex_t(teardown_task_nums, self%description_ // ": teardown")]
            child_task_nums = [this_task, teardown_task_nums, size(vertices)]
        else
            child_task_nums = [this_task, teardown_task_nums]
        end if
    end subroutine
end module
