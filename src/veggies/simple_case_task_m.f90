module veggies_simple_case_task_m
    use iso_varying_string, only: varying_string, operator(//), put_line
    use payload_m, only: payload_t
    use strff, only: to_string
    use task_m, only: task_t
    use veggies_command_line_m, only: DEBUG
    use veggies_test_case_result_m, only: test_case_result_t
    use veggies_test_interfaces_m, only: computation_i, simple_test_i
    use veggies_test_result_item_m, only: test_result_item_t

    implicit none
    private
    public :: simple_case_task_t

    type, extends(task_t) :: simple_case_task_t
        private
        type(varying_string) :: description_
        procedure(simple_test_i), nopass, pointer :: test
        logical :: has_setup_and_teardown
        procedure(computation_i), nopass, pointer :: setup
        procedure(computation_i), nopass, pointer :: teardown
    contains
        procedure :: execute
    end type

    interface simple_case_task_t
        module procedure constructor_basic
        module procedure constructor_bracketed
    end interface
contains
    function constructor_basic(description, test) result(simple_case_task)
        type(varying_string), intent(in) :: description
        procedure(simple_test_i) :: test
        type(simple_case_task_t) :: simple_case_task

        simple_case_task%description_ = description
        simple_case_task%test => test
        simple_case_task%has_setup_and_teardown = .false.
    end function

    function constructor_bracketed( &
            description, test, setup, teardown) result(simple_case_task)
        type(varying_string), intent(in) :: description
        procedure(simple_test_i) :: test
        procedure(computation_i) :: setup
        procedure(computation_i) :: teardown
        type(simple_case_task_t) :: simple_case_task

        simple_case_task%description_ = description
        simple_case_task%test => test
        simple_case_task%has_setup_and_teardown = .true.
        simple_case_task%setup => setup
        simple_case_task%teardown => teardown
    end function

    function execute(self, arguments) result(output)
        class(simple_case_task_t), intent(in) :: self
        type(payload_t), intent(in) :: arguments(:)
        type(payload_t) :: output

        type(test_result_item_t) :: result_

        associate(unused => arguments)
        end associate

        if (DEBUG) call put_line( &
                "Beginning execution of: " // self%description_ &
                // " on image " // to_string(this_image()))
        if (self%has_setup_and_teardown) call self%setup
        result_ = test_result_item_t(test_case_result_t( &
                self%description_, self%test()))
        output = payload_t(result_%serialize())
        if (self%has_setup_and_teardown) call self%teardown
        if (DEBUG) call put_line( &
                "Completed execution of: " // self%description_ &
                // " on image " // to_string(this_image()))
    end function
end module
