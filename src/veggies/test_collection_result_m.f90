module veggies_test_collection_result_m
    use iso_varying_string, only: &
            varying_string, assignment(=), operator(//), char, len
    use strff, only: add_hanging_indentation, join, NEWLINE
    use veggies_constants_m, only: INDENTATION
    use veggies_test_result_m, only: test_result_t
    use veggies_test_result_item_m, only: test_result_item_t

    implicit none
    private
    public :: test_collection_result_t

    type, extends(test_result_t) :: test_collection_result_t
        private
        type(varying_string) :: description
        type(test_result_item_t), allocatable :: results(:)
    contains
        private
        procedure, public :: num_asserts
        procedure, public :: num_cases
        procedure, public :: num_failing_asserts
        procedure, public :: num_failing_cases
        procedure, public :: passed
        procedure, public :: failure_description
        procedure, public :: verbose_description
        procedure, public :: serialize
    end type

    interface test_collection_result_t
        module procedure constructor
        module procedure deserialize
    end interface
contains
    pure function constructor(description, results) result(test_collection_result)
        type(varying_string), intent(in) :: description
        type(test_result_item_t), intent(in) :: results(:)
        type(test_collection_result_t) :: test_collection_result

        test_collection_result%description = description
        allocate(test_collection_result%results, source = results)
    end function


    recursive function deserialize(string) result(test_collection_result)
        ! Format
        ! total_length, type_tag, description_length, description, num_results, each_result...
        character(len=*), intent(in) :: string
        type(test_collection_result_t) :: test_collection_result

        integer :: i
        integer :: next_end
        integer :: next_len
        integer :: next_start
        integer :: num_results
        integer :: description_length

        read(string(13:23), '(I11)') description_length
        test_collection_result%description = string(24:description_length+23)
        read(string(description_length+24:description_length+34), '(I11)') num_results
        allocate(test_collection_result%results(num_results))
        next_start = description_length+35
        do i = 1, num_results
            read(string(next_start:next_start+10), '(I11)') next_len
            next_end = next_start + next_len - 1
            test_collection_result%results(i) = test_result_item_t(string(next_start:next_end))
            next_start = next_end + 1
        end do
    end function

    pure recursive function failure_description( &
            self, colorize) result(description)
        class(test_collection_result_t), intent(in) :: self
        logical, intent(in) :: colorize
        type(varying_string) :: description

        logical, allocatable :: failed(:)
        type(varying_string), allocatable :: failed_messages(:)
        integer :: num_failed
        integer :: i, j

        if (self%passed()) then
            description = ""
        else
            failed = [(.not.self%results(i)%passed(), i = 1, size(self%results))]
            num_failed = count(failed)
            j= 1
            allocate(failed_messages(num_failed))
            do i = 1, size(failed)
                if (failed(i)) then
                    failed_messages(j) = self%results(i)%failure_description(colorize)
                    j = j + 1
                    if (j > num_failed) exit
                end if
            end do
            description = add_hanging_indentation( &
                    self%description // NEWLINE // join( &
                            failed_messages, &
                            NEWLINE), &
                    INDENTATION)
        end if
    end function

    pure recursive function num_asserts(self)
        class(test_collection_result_t), intent(in) :: self
        integer :: num_asserts

        integer :: i

        num_asserts = sum([(self%results(i)%num_asserts(), i = 1, size(self%results))])
    end function

    pure recursive function num_cases(self)
        class(test_collection_result_t), intent(in) :: self
        integer :: num_cases

        integer :: i

        num_cases = sum([(self%results(i)%num_cases(), i = 1, size(self%results))])
    end function

    pure recursive function num_failing_asserts(self) result(num_asserts)
        class(test_collection_result_t), intent(in) :: self
        integer :: num_asserts

        integer :: i

        num_asserts = sum([(self%results(i)%num_failing_asserts(), i = 1, size(self%results))])
    end function

    pure recursive function num_failing_cases(self) result(num_cases)
        class(test_collection_result_t), intent(in) :: self
        integer :: num_cases

        integer :: i

        num_cases = sum([(self%results(i)%num_failing_cases(), i = 1, size(self%results))])
    end function

    pure recursive function passed(self)
        class(test_collection_result_t), intent(in) :: self
        logical :: passed

        integer :: i

        passed = all([(self%results(i)%passed(), i = 1, size(self%results))])
    end function

    pure recursive function verbose_description( &
            self, colorize) result(description)
        class(test_collection_result_t), intent(in) :: self
        logical, intent(in) :: colorize
        type(varying_string) :: description

        integer :: i

        description = add_hanging_indentation( &
                self%description // NEWLINE // join( &
                        [(self%results(i)%verbose_description(colorize), i = 1, size(self%results))], &
                        NEWLINE), &
                INDENTATION)
    end function

    pure recursive function serialize(self) result(string)
        ! Format
        ! total_length, type_tag, description_length, description, num_results, each_result...
        class(test_collection_result_t), intent(in) :: self
        character(len=:), allocatable :: string

        character(len=*), parameter :: type_tag = "M"
        integer :: description_length
        character(len=11) :: description_length_part
        integer :: i
        integer :: num_results
        character(len=11) :: num_results_part
        character(len=:), allocatable :: results_part
        integer :: total_length
        character(len=11) :: total_length_part

        num_results = size(self%results)
        results_part = ""
        do i = 1, num_results
            results_part = results_part // self%results(i)%serialize()
        end do
        description_length = len(self%description)
        total_length = 11*3 + 1 + description_length + len(results_part)
        write(total_length_part, '(I11)') total_length
        write(description_length_part, '(I11)') description_length
        write(num_results_part, '(I11)') num_results
        string = &
                total_length_part &
                // type_tag &
                // description_length_part &
                // char(self%description) &
                // num_results_part &
                // results_part
    end function
end module
