module veggies_transformation_failure_m
    use veggies_input_m, only: input_t, deserialize_i
    use veggies_result_m, only: result_t

    implicit none
    private
    public :: transformation_failure_t

    type, extends(input_t) :: transformation_failure_t
        private
        type(result_t) :: result__
    contains
        private
        procedure, public :: result_
        procedure, public :: serialize
        procedure, public :: get_deserializer
    end type

    interface transformation_failure_t
        module procedure constructor
    end interface
contains
    pure function constructor(result_) result(transformation_failure)
        type(result_t), intent(in) :: result_
        type(transformation_failure_t) :: transformation_failure

        transformation_failure%result__ = result_
    end function

    pure function result_(self)
        class(transformation_failure_t), intent(in) :: self
        type(result_t) :: result_

        result_ = self%result__
    end function

    pure function serialize(self) result(string)
        class(transformation_failure_t), intent(in) :: self
        character(len=:), allocatable :: string

        string = self%result__%serialize()
    end function

    pure function deserialize(string) result(input)
        character(len=*), intent(in) :: string
        class(input_t), allocatable :: input

        type(transformation_failure_t) :: local_input

        local_input%result__ = result_t(string)
        input = local_input
    end function

    pure function get_deserializer(self) result(deserializer)
        class(transformation_failure_t), intent(in) :: self
        procedure(deserialize_i), pointer :: deserializer

        associate(unused => self); end associate
        deserializer => deserialize
    end function
end module
