module veggies_string_input_m
    use iso_varying_string, only: varying_string, assignment(=), char
    use veggies_input_m, only: input_t, deserialize_i

    implicit none
    private
    public :: string_input_t

    type, extends(input_t) :: string_input_t
        private
        type(varying_string) :: input_
    contains
        private
        procedure, public :: input
        procedure, public :: serialize
        procedure, public :: get_deserializer
    end type

    interface string_input_t
        module procedure constructor
    end interface
contains
    pure function constructor(input) result(string_input)
        type(varying_string), intent(in) :: input
        type(string_input_t) :: string_input

        string_input%input_ = input
    end function

    pure function input(self)
        class(string_input_t), intent(in) :: self
        type(varying_string) :: input

        input = self%input_
    end function

    pure function serialize(self) result(string)
        class(string_input_t), intent(in) :: self
        character(len=:), allocatable :: string

        string = char(self%input_)
    end function

    pure function deserialize(string) result(input)
        character(len=*), intent(in) :: string
        class(input_t), allocatable :: input

        type(string_input_t) :: local_input

        local_input%input_ = string
        input = local_input
    end function

    pure function get_deserializer(self) result(deserializer)
        class(string_input_t), intent(in) :: self
        procedure(deserialize_i), pointer :: deserializer

        associate(unused => self); end associate
        deserializer => deserialize
    end function
end module
